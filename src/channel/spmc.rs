use super::{EventFd, MReadIdx, NullTokenFactory, SWriteIdx, TokenArray};
use crate::Result;
use hipool::{Allocator, PoolAlloc};

type Pipe<'a, T> = super::pipe::Pipe<'a, T, SWriteIdx, MReadIdx, EventFd>;
pub type Receiver<'a, T, A = PoolAlloc> = super::pipe::Receiver<'a, T, TokenArray, Pipe<'a, T>, A>;
pub type Sender<'a, T, A = PoolAlloc> =
    super::pipe::Sender<'a, T, NullTokenFactory, Pipe<'a, T>, A>;
pub type Channel<'a, T, A = PoolAlloc> = (Sender<'a, T, A>, Receiver<'a, T, A>);

pub fn channel<T>(len: usize) -> Result<Channel<'static, T>> {
    channel_in(&PoolAlloc, len)
}

pub fn channel_in<'a, T, A>(alloc: &'a A, len: usize) -> Result<Channel<'a, T, A>>
where
    A: Allocator
{
    let pipe = Pipe::<'a, T>::new_in(alloc, len)?;
    Ok((Sender::new(pipe.clone()), Receiver::new(pipe.clone())))
}
