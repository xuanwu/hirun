use hicollections::{list, List, ListNode};
use super::Scheduler;

#[repr(C)]
pub struct Event {
    pub(crate) node: ListNode,
    handle: fn(&Event, u32, u16, &mut dyn Scheduler),
}

pub fn event_list_new() -> List<Event> {
    list!(Event, node)
}

impl Event {
    pub fn new(handle: fn(&Event, u32, u16, &mut dyn Scheduler)) -> Self {
        Self {
            node: ListNode::new(),
            handle,
        }
    }

    pub fn handle(&self, event: u32, flags: u16, sched: &mut dyn Scheduler) {
        (self.handle)(self, event, flags, sched)
    }
}
