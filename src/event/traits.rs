use super::{Event, Timer};
use crate::Result;
use core::time::Duration;

pub const POLLIN: u32 = 0x01; //libc::EPOLLIN
pub const POLLOUT: u32 = 0x04; //libc::EPOLLOUT
pub const POLLET: u32 = 0x8000_0000; // libc::EPOLLET
pub const POLLONESHOT: u32 = 0x4000_0000; //libc::EPOLLONESHOT

pub struct PollEvent(pub u32, pub u64);

pub trait Poll {
    fn add_event(&mut self, fd: i32, events: u32, e: u64) -> Result<()>;
    fn mod_event(&mut self, fd: i32, events: u32, e: u64) -> Result<()>;
    fn del_event(&mut self, fd: i32) -> Result<()>;
    fn wait(&mut self, timeout: i32) -> impl Iterator<Item = PollEvent> + '_;
}

pub trait Scheduler {
    unsafe fn add_event(&mut self, e: &Event, priority: i32);
    unsafe fn del_event(&mut self, e: &Event);
    unsafe fn add_fd_event(&mut self, e: &Event, events: u32, fd: i32, flags: u16) -> Result<()>;
    unsafe fn mod_fd_event(&mut self, e: &Event, events: u32, fd: i32, flags: u16) -> Result<()>;
    unsafe fn del_fd_event(&mut self, fd: i32) -> Result<()>;
    unsafe fn set_timer(&mut self, t: &Timer, msecs: u32);
    unsafe fn del_timer(&mut self, t: &Timer);
    fn private_data(&self) -> *const ();
    fn set_private_data(&mut self, private: *const ());
    fn stop(&self);
    fn now(&self) -> Duration;
}
