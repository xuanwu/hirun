
mod addr;
pub use addr::*;

mod fd;
pub use fd::*;

mod aio;
pub use aio::*;

mod socket;
pub use socket::*;

mod iov;
use iov::*;

pub(crate) fn const_buf(buf: &[u8]) -> *const libc::c_void {
    buf.as_ptr() as *const libc::c_void
}
pub(crate) fn mut_buf(buf: &mut [u8]) -> *mut libc::c_void {
    buf.as_mut_ptr() as *mut libc::c_void
}

pub(crate) fn const_void<T>(val: &T) -> *const libc::c_void {
    val as *const T as *const libc::c_void
}
pub(crate) fn mut_void<T>(val: &mut T) -> *mut libc::c_void {
    val as *mut T as *mut libc::c_void
}
