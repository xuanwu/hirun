
cfg_if::cfg_if! {
    if #[cfg(unix)] {
        pub const EINVAL: i32 = hierr::EINVAL;
        pub const ESHUTDOWN: i32 = hierr::ESHUTDOWN;
        pub const ECANCELED: i32 = hierr::ECANCELED;
    } else if #[cfg(windows)] {
        pub const EINVAL: i32 = hierr::WSA_INVALID_PARAMETER;
        pub const ESHUTDOWN: i32 = hierr::WSAESHUTDOWN;
        pub const ECANCELED: i32 = hierr::WSACANCELLED;
    }
}
