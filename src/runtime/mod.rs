mod env;
pub use env::*;

mod attr;
pub use attr::*;

mod future;
pub use future::*;

mod waker;

mod context;
pub use context::*;

pub mod local;
pub mod task;
pub(crate) use task::raw::*;
pub(crate) use task::task::*;
pub(crate) use task::fdset::*;

mod pool_cache;
pub(crate) use pool_cache::*;

mod join;
pub use join::*;

mod queue;
pub(crate) use queue::*;

mod builder;
pub use builder::*;

#[allow(clippy::module_inception)]
mod runtime;
pub(crate) use runtime::*;

mod active_worker;
pub(crate) use active_worker::*;

mod group;
pub use group::Group as Runtime;
pub(crate) use group::*;

mod sender;
pub(crate) use sender::*;

mod sched;
pub use sched::*;

mod mutex;
pub use mutex::*;

use core::mem::ManuallyDrop;
use core::ops::{Deref, DerefMut};
use core::ptr::NonNull;

pub(crate) struct TaskRef {
    task: NonNull<RawTask>,
}

unsafe impl Send for TaskRef {}

impl Drop for TaskRef {
    fn drop(&mut self) {
        self.dec_ref();
    }
}

impl Clone for TaskRef {
    fn clone(&self) -> Self {
        self.inc_ref();
        Self { task: self.task }
    }
}

impl TaskRef {
    pub fn new(task: NonNull<RawTask>) -> Self {
        Self { task }
    }
    /// # Safety
    /// 使用人员保证task来自Self::leak
    pub unsafe fn from(task: *mut RawTask) -> Self {
        Self {
            task: NonNull::new_unchecked(task),
        }
    }
    pub fn leak(self) -> *mut RawTask {
        ManuallyDrop::new(self).task.as_ptr()
    }
}

impl Deref for TaskRef {
    type Target = RawTask;
    fn deref(&self) -> &Self::Target {
        unsafe { self.task.as_ref() }
    }
}

impl DerefMut for TaskRef {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { self.task.as_mut() }
    }
}
