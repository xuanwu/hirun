use super::Runtime;
use crate::Result;

#[derive(Clone)]
pub(crate) struct Config {
    pub(crate) qlen: usize,
    pub(crate) nth: usize,
    pub(crate) max_cache: usize,
    pub(crate) id: u8,
    pub(crate) name: &'static str,
}
pub(crate) const THREADS_MAX: usize = 1024;
pub(crate) const THREADS_MASK: usize = THREADS_MAX - 1;

pub struct Builder {
    conf: Config,
}

impl Config {
    pub(crate) const fn new(id: u8) -> Self {
        Self {
            qlen: 0,
            nth: 0,
            max_cache: 0,
            id,
            name: "",
        }
    }
}

impl Builder {
    /// 参数id代表运行时的id
    pub const fn new() -> Self {
        Self {
            conf: Config::new(0),
        }
    }

    /// 运行时的ID, 缺省运行时的ID为0. 每个id对应一个独立的运行时实例, 和Attr::id保持一致.
    pub fn id(&mut self, id: u8) -> &mut Self {
        self.conf.id = id;
        self
    }

    /// 运行时的名字，是此运行时所有工作线程名字的前缀
    pub fn name(&mut self, name: &'static str) -> &mut Self {
        self.conf.name = name;
        self
    }

    /// 每个worker可以缓存的task最大数量. 缺省为0, 不缓存.
    pub fn max_cache(&mut self, max_cache: usize) -> &mut Self {
        self.conf.max_cache = max_cache;
        self
    }

    /// 设置运行时内部mpsc队列的大小，这个队列并非用于task调度，仅用于控制消息.
    /// 任务调度的队列没有大小限制
    pub fn qlen(&mut self, len: usize) -> &mut Self {
        self.conf.qlen = len;
        self
    }

    /// 设置运行时的工作线程的数量，缺省为当前CPU核的数量.
    pub fn nth(&mut self, nth: usize) -> &mut Self {
        if nth < THREADS_MAX {
            self.conf.nth = nth;
        } else {
            self.conf.nth = THREADS_MAX;
        }
        self
    }

    /// 启动运行时，如果id对应的运行时已经启动，则会返回Err.
    pub fn build(&mut self) -> Result<()> {
        Runtime::build(&self.conf)
    }
}
