use super::{Attr, RawTask, TaskPool, TaskRef};
use crate::event::Scheduler;
use crate::Result;
use core::future::Future;
use core::ptr::NonNull;
use hipool::MemPool;

pub(crate) trait Worker: Scheduler {
    fn spawn<T: Future>(&mut self, future: T, attr: &Attr) -> Result<TaskRef>;
    fn spawn_local<T: Future>(&mut self, future: T, attr: &Attr) -> Result<TaskRef>;
    fn wake(&mut self, task: TaskRef, local: Option<u16>);
    fn set_current_task(&mut self, current: Option<NonNull<RawTask>>);
    fn sched_local(&mut self, task: TaskRef);
    fn cache_task_pool(&mut self, pool: TaskPool);
    fn group_id(&self) -> u8;
    fn worker_id(&self) -> u16;
    fn pool(&self) -> &'static MemPool;
}
