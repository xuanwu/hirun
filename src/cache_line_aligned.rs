use core::default::Default;
use core::fmt::{self, Debug, Display};
use core::ops::{Deref, DerefMut};

#[cfg_attr(
    any(
        target_arch = "x86_64",
        target_arch = "aarch64",
        target_arch = "powerpc64",
    ),
    repr(align(128))
)]
#[cfg_attr(
    any(
        target_arch = "arm",
        target_arch = "mips",
        target_arch = "mips64",
        target_arch = "sparc",
        target_arch = "hexagon",
    ),
    repr(align(32))
)]
#[cfg_attr(target_arch = "m68k", repr(align(16)))]
#[cfg_attr(target_arch = "s390x", repr(align(256)))]
#[cfg_attr(not(any(
        target_arch = "x86_64",
        target_arch = "aarch64",
        target_arch = "powerpc64",
        target_arch = "arm",
        target_arch = "mips",
        target_arch = "mips64",
        target_arch = "sparc",
        target_arch = "hexagon",
        target_arch = "m68k",
        target_arch = "s390x",
)), repr(align(64)))]
pub(crate) struct CacheLineAligned<T>(pub T);

impl<T> CacheLineAligned<T> {
    pub fn new(val: T) -> Self {
        Self(val)
    }
}

impl<T: Default> Default for CacheLineAligned<T> {
    fn default() -> Self {
        Self(T::default())
    }
}

impl<T> Deref for CacheLineAligned<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> DerefMut for CacheLineAligned<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl<T: Copy> Copy for CacheLineAligned<T> {}

impl<T: Clone> Clone for CacheLineAligned<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<T: Display> Display for CacheLineAligned<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Display::fmt(&self.0, f)
    }
}

impl<T: Debug> Debug for CacheLineAligned<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Debug::fmt(&self.0, f)
    }
}
