
use core::ptr;
use core::time::Duration;

//
// libc crate don't support c11 timespec_get
//

const TIME_UTC: libc::c_int = 1;

pub fn now() -> Duration {
    let mut now = libc::timespec {
        tv_sec: 0,
        tv_nsec: 0,
    };

    let _ = unsafe { timespec_get(ptr::addr_of_mut!(now), TIME_UTC) };
    Duration::new(now.tv_sec as u64, now.tv_nsec as u32)
}

extern "C" {
    fn timespec_get(tm: *mut libc::timespec, base: i32) -> i32;
}
