/// 根据需要延迟调用指定Fn，缺省在析构的时候调用.
pub struct Guard<F: FnOnce()> {
    action: Option<F>,
}

impl<F: FnOnce()> Guard<F> {
    /// 指定待调用的Fn.
    pub fn new(action: F) -> Self {
        Self { action: Some(action) }
    }
    /// 主动调用new传递的Fn.
    pub fn commit(self) {}
    
    /// 放弃调用new传递的Fn.
    pub fn dismiss(mut self) {
        let _ = self.action.take();
    }
}

impl<F: FnOnce()> Drop for Guard<F> {
    fn drop(&mut self) {
        if let Some(action) = self.action.take() {
            action()
        }
    }
}
